document.namespace = (target, name, block) ->
  [target, name, block] = [(if typeof exports isnt 'undefined' then exports else window), arguments...] if arguments.length < 3
  top    = target
  target = target[item] or= {} for item in name.split '.'
  block target, top

document.randomInt = (min, max) ->
  return Math.floor(Math.random() * (max - min + 1)) + min
