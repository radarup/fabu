document.namespace "THV.landing.request", (exports) ->
  exports.setup = ->
    $("form.request").bind "ajax:success",  THV.landing.request.success
    $("form.request").bind "ajax:error", THV.landing.request.error
    $("form.request").bind "ajax:beforeSend", THV.landing.request.beforeSend

  exports.beforeSend = (event, xhr, settings) ->
    _gaq.push(['_trackEvent', "landing", "signup"])
    $("#request_submit").attr("disabled", true)
    settings.data += "&user[referer]=" + document.referrer

  exports.success = (event, data) ->
    THV.landing.invite.loadPage(data)
    return false

  exports.error = ->
    window.setTimeout(->
      $("#request_submit").attr("disabled", false)
    $("#error").fadeIn()
      window.setTimeout(->
        $("#error").fadeOut()
      ,5000)
    , 1000)
    return false

document.namespace "THV.landing.invite", (exports) ->
  exports.setup = ->
    $("form.invite").bind "ajax:success", THV.landing.invite.success
    $("form.invite").bind "ajax:beforeSend", THV.landing.invite.beforeSend
    $("form.invite").bind "ajax:error", THV.landing.invite.error
    $(".skip").click(THV.landing.share.loadPage)
    $(".skip").click(-> _gaq.push(['_trackEvent', "landing", "skip"]))

  exports.loadPage = (data) ->
    $.ajax({
      url: "/users/" + data.uid + "/invitations",
      data: {layout: false}
    }).done((data) ->
      $("#signup").after(data)
      THV.landing.invite.setup()
      $("#invite").fadeIn()
      $("#signup").hide()
    )

  exports.beforeSend = (event, xhr, settings) ->
    _gaq.push(['_trackEvent', "landing", "invite"])
    $("#invite_submit").attr("disabled", true)

  exports.success = (event, data) ->
    THV.landing.share.loadPage(data)
    return false

  exports.error = (event, data) ->
    console.log("some error?")

document.namespace "THV.landing.share", (exports) ->
  exports.loadPage = (data) ->
    $.ajax({
    url: "/users/" + $(".skip").data("user-uid") + "/share",
    data: {layout: false}
    }).done((data) ->
      $("#invite").after(data)
      $("#ulink").focus(->_gaq.push(['_trackEvent', "landing", "link_copy"]))
      $("#ulink").click(->this.select())
      $(".twbutton").click(-> _gaq.push(['_trackEvent', "landing", "twshare"]))
      $(".fbbutton").click(-> _gaq.push(['_trackEvent', "landing", "fbshare"]))
      $("#share").fadeIn()
      $("#invite").hide()
    )

$(document).ready ->
  $("body").attr("style", "background-image:url(\""+"assets/bg"+document.randomInt(1, 9)+".jpg\");")
  THV.landing.request.setup()
  THV.landing.invite.setup()
