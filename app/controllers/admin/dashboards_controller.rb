class Admin::DashboardsController < ApplicationController
  def show
    @users = User.order("created_at DESC").all
    render layout: "admin"
  end
end
