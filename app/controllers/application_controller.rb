class ApplicationController < ActionController::Base
  protect_from_forgery
  #before_filter :authenticate_for_beta

  private

  def set_cookie(name, value)
    cookies[name] = {:value => value, :domain => :all}
  end

  def authenticate_for_beta
    return if cookies['beta_access'] == 'true'

    result = authenticate_or_request_with_http_basic do |username, password|
      username == "hippy" && password == "hippy"
    end

    set_cookie('beta_access', 'true') if result == true
    result
  end

end
