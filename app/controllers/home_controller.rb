class HomeController < ApplicationController
  def index
    @uid = params[:user_uid]
    @user = User.find_by_id(params[:user_id].to_i)
    unless @user
      @user = User.new
      3.times{@user.inviteds.build}
    end
  end
end
