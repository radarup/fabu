class InvitationsController < ApplicationController
  def index
    @user = User.find_by_uid(params[:user_id])
    (User::TOTAL_INVITATIONS - @user.invitations.count).times { @user.inviteds.build }
    render :layout => 'invitations'
  end

  def create
  end

end
