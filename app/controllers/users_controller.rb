class UsersController < ApplicationController
  def create
    user = User.new(params[:user])
    if user.save!
      referal_user = User.select(:id).where(uid: params[:referal_uid]).first
      if referal_user
        Invitation.create(inviter_id: referal_user.id, invited_id: user.id, invitation_type: Invitation::LINK)
      end
      render :json => user
    else
      head :bad_request
    end
  end

  def update
    user = User.find_by_uid(params[:id])
    user.update_attributes(params[:user])
    render :json => user
  end

  def share
    @user = User.find_by_uid(params[:id])
    layout = params[:layout] == "false" ? false : "landing"
    render layout: layout
  end
end
