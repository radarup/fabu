class UserMailer < ActionMailer::Base
  default from: "Hipvan <hello@hipvan.com>"

  def requested_email(user)
    @user = user
    @url = "whatever.com/here"
    mail(to: user.email, subject: "Your Membership Request for Hipvan Private Sales")
  end

  def approved_email(user)
    @user = user
    @url = "whatever.com/here"
    mail(to: user.email, subject: "Hipvan.com Membership Approved - Welcome to Hipvan.com")
  end

  def invited_email(user)
    @user = user
    @inviter = user.inviter
    @url = "whatever.com/here"
    mail(to: user.email, subject: "Invite from #{@inviter.email} to Hipvan.com - design inspirations for everyday living")
  end
end
