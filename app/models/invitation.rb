class Invitation < ActiveRecord::Base
  belongs_to :inviter, class_name: "User"
  belongs_to :invited, class_name: "User"

  #invitation types
  LINK = "link"
  EMAIL = "email"

  before_create :set_default_invitation_type
  after_create :send_welcome_email

  private
  def set_default_invitation_type
    self[:invitation_type] = Invitation::EMAIL unless self.invitation_type
  end


  def send_welcome_email
    if self.invited.state == User::INVITED
      UserMailer.invited_email(self.invited).deliver
    end
  end

end
