class User < ActiveRecord::Base
  has_many :invitations, :foreign_key => :inviter_id
  has_many :inviteds, :through => :invitations
  has_one :invitation, :foreign_key => :invited_id
  has_one :inviter, :through => :invitation

  validates :email, :uid, :uniqueness => true
  accepts_nested_attributes_for :inviteds, :reject_if => lambda { |a| a[:email].blank? }

  before_create :set_uid
  after_create :add_to_mailchimp, :send_welcome_email

  #User states
  INVITED = "invited"
  REQUESTED = "requested"
  APPROVED = "approved"
  CONFIRMED = "confirmed"

  TOTAL_INVITATIONS = 1

  def to_param
    self.uid
  end

  def self.accept_requests
    n = 0
    self.find_all_by_state(REQUESTED).each do |u|
      if u.created_at < 1.day.ago
        u.state = APPROVED
        u.save
        UserMailer.approved_email(u).deliver
        n += 1
      end
    end
    puts "#{n} emails sent"
  end

  private

  def self.generate_uid(size = 5)
    chars = ('a'..'z').to_a + ('A'..'Z').to_a
    (0...size).collect { chars[Kernel.rand(chars.length)] }.join
  end

  def set_uid
    tmp_uid = User.generate_uid
    while User.find_by_uid(tmp_uid)
      tmp_uid = User.generate_uid
    end
    self.uid = tmp_uid
  end

  def add_to_mailchimp
    begin
      if self.state == REQUESTED
        h = Hominid::API.new(ENV["MAILCHIMP_API"])
        h.list_subscribe(ENV["MAILCHIMP_LIST"], self.email, {}, 'html', false, true, true, false)
      end
    rescue Exception => e
      puts e.message
    end

  end

  def send_welcome_email
    if self.state == REQUESTED
        UserMailer.requested_email(self).deliver
    end
  end

end
