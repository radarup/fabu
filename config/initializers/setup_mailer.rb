
if Rails.env.development?
  ActionMailer::Base.delivery_method = :sendmail
  ActionMailer::Base.default_url_options[:host] = "localhost:3000"
  Mail.register_interceptor(DevelopmentMailInterceptor)
end

if Rails.env.production?
  ENV['SENDGRID_USERNAME'] = "app3642998@heroku.com"
  ENV['SENDGRID_PASSWORD'] = "olzei0cf"

  ActionMailer::Base.smtp_settings = {
      :address        => 'smtp.sendgrid.net',
      :port           => '587',
      :authentication => :plain,
      :user_name      => ENV['SENDGRID_USERNAME'],
      :password       => ENV['SENDGRID_PASSWORD'],
      :domain         => 'heroku.com'
  }
  ActionMailer::Base.delivery_method = :smtp
end
