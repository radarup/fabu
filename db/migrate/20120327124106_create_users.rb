class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :state
      t.integer :invitations
      t.datetime :state_update

      t.timestamps
    end
  end
end
