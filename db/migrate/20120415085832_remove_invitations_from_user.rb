class RemoveInvitationsFromUser < ActiveRecord::Migration
  def up
    remove_column :users, :invitations
  end

  def down
    add_column :users, :invitations, :integer
  end
end
