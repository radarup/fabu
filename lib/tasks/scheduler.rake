desc "This task is called by the Heroku scheduler add-on"

task :accept_requests => :environment do
  puts "Accepting requests..."
  User.accept_requests
  puts "done."
end